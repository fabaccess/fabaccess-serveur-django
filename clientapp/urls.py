from django.urls import path
from . import views
from django.views.decorators.csrf import csrf_exempt


app_name = 'clientapp'
urlpatterns = [
    path(r'', views.Accueil.as_view(), name='index'),
    path(r'aide', views.Aide.as_view(), name='aide'),
    path(r'bienvenu', csrf_exempt(views.ArriveeDepartUtilisateur.as_view()), name='welcome'),
    path(r'accueilJS', views.AccueilJS.as_view(), name='indexJS'),
    path(r'connexion', csrf_exempt(views.ClientLogin.as_view()), name='log'),
    path(r'message', views.MessageAccueil.as_view(), name='message'),
    path(r'user', views.Profil.as_view(), name='user'),
    path(r'statistiques', views.Stats.as_view(), name='stats'),
    path(r'machine', views.ListeMachines.as_view(), name="machineuser"),
    path(r'panne/<int:pk>', views.DeclarationPanne.as_view(), name="panne"),
    path(r'panneview/<int:pk>', views.PanneView.as_view(), name="panneview"),

    path(r'borneclient',views.Websocket.as_view(),name="borneclient")
]

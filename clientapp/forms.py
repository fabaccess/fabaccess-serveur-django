from django import forms

from fabaccess.models import Panne

class IncidentReportForm(forms.ModelForm):

    class Meta:
        model = Panne
        fields = ('machineId', 'cause')

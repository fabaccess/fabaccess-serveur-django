from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import login
from django.views.decorators.csrf import csrf_exempt
from django.views import View
import datetime
from fabaccess.models import *
import time
from django.utils.timezone import make_aware
# Create your views here.

from configuration.settings import *

#page d'accueil
class Accueil(View):
    def get(self,request):
        context = {
            'duree_timer': 60,
            'redirection_timer': 'panel/logout'
        }
        template = loader.get_template('clientapp/index.html')

        return HttpResponse(template.render(context, request))

#page d'aide
class Aide(View):
    def get(self,request):
        context = {
            'duree_timer': 60,
            'redirection_timer': 'panel/logout'
        }
        template = loader.get_template('clientapp/aide.html')
        return HttpResponse(template.render(context, request))

class AccueilJS(View):
    def get(self,request):
        context = {
            'script':'ok',
            'duree_timer': 60,
            'redirection_timer': 'panel/logout'
        }
        template = loader.get_template('clientapp/index.html')

        return HttpResponse(template.render(context, request))

#page test de websocket
class Websocket(View):
    def get(self,request):

        # test=[DEFAULT_FROM_EMAIL, EMAIL_HOST_PASSWORD, EMAIL_HOST_USER, EMAIL_PORT, EMAIL_HOST, EMAIL_BACKEND ]
        # return HttpResponse(test)
        return render(request, 'clientapp/choixAccesSiteOuDepart.html')



class ClientLogin(View):
    @csrf_exempt
    def get(self, request):
        idbadge = request.GET['badgetag']
        user = User.objects.filter(userdata__tagbadge__startswith=idbadge).get()
        login(request, user)
        if user.userdata.type == 'User':
            login(request, user)
            return redirect('clientapp:indexJS')
        else:
            return redirect('adminpanel:index')

#message d'accueil
class MessageAccueil(View):
    def get(self,request):
        badgetag=request.GET['badgetag']
        user = User.objects.filter(userdata__tagbadge__exact=badgetag)
        if not user:
            return HttpResponse('Badge invalide')
        else:
            user = User.objects.filter(userdata__tagbadge__exact=badgetag).get()
            if (user.userdata.type == 'Fabmanager' or user.userdata.type == 'Concierge'):
                return HttpResponse('Bonjour '+ user.first_name)
            elif user.userdata.online==0:
                return HttpResponse('Bonjour ' + user.first_name)
            else:
                return HttpResponse('Au Revoir ' + user.first_name)


#page d'accueil en cas d'utilisation d'un badge rfid pour s'y connecter
#si administrateur connection au compte admin et passage sur le pannel admin
#si user on ne connecte pas à proprement parlé mais on modifié la variable en ligne dan user data
class ArriveeDepartUtilisateur(View):
    def get(self,request):
        badgetag =request.GET['badgetag']
        user1 = User.objects.filter(userdata__tagbadge__exact=badgetag)
        if not user1:
            context = {
                'msg': "Badge invalide. Appelez le FabManager",
                'duree_timer': 5,
                'redirection_timer': ''
            }
            template = loader.get_template('clientapp/error.html')

            return HttpResponse(template.render(context, request))
        else:
            user1 = User.objects.filter(userdata__tagbadge__exact=badgetag).get()
            clef=ClefAPI.objects.get(nom="doliactive")
            if clef.clef=='1':
                now = make_aware(datetime.datetime.now())
                if (user1.userdata.type == 'Fabmanager' or user1.userdata.type == 'Concierge'):
                    return redirect('adminpanel:log', badgetag=badgetag)
                elif (user1.userdata.subscription == 0 or user1.userdata.subscription < int(time.time())):
                    context = {
                        'msg':"Votre cotisation n'est pas à jour. Appelez le FabManager.",
                        'duree_timer': 5,
                        'redirection_timer': ''
                    }
                    template = loader.get_template('clientapp/error.html')
                    return HttpResponse(template.render(context, request))

                else:
                    if (user1.userdata.type == 'Fabmanager' or user1.userdata.type == 'Concierge'):

                        return redirect('adminpanel:log', badgetag=badgetag)
                    else:
                        if user1.userdata.online == 0:
                            if user1.userdata.active == 0:
                                context ={
                                    'msg':"Votre compte n'est pas activé. Appelez le FabManager.",
                                    'duree_timer': 5,
                                    'redirection_timer': ''
                                }
                                template = loader.get_template('clientapp/error.html')
                                return HttpResponse(template.render(context, request))

                            else:
                                user1.userdata.online = 1
                                user1.save()

                                now = make_aware(datetime.datetime.now())
                                registre = RegistreEntree(username=user1.userdata, debut=now)
                                registre.save()

                                msg=user1.userdata.message
                                user1.userdata.message=None
                                user1.save()

                                machines = Machine.objects.all()
                                listemachines = []
                                for machine in machines:
                                    droit = Droit.objects.filter(username=user1.userdata).filter(
                                        machineId=machine).get()
                                    machine.droit = droit.typeAccess
                                    if machine.libre == 0:
                                        registre = RegistreMachine.objects.all().filter(machineId=machine).last()
                                        machine.user = registre.username
                                    listemachines.append(machine)
                                context = {
                                    "user1": user1,
                                    'msg': msg,
                                    'duree_timer': 30,
                                    'listemachines': listemachines,
                                    'redirection_timer': ''
                                }
                                template = loader.get_template('clientapp/welcome.html')
                                return HttpResponse(template.render(context, request))



                        else:
                            user1.userdata.online = 0
                            user1.save()

                            now = make_aware(datetime.datetime.now())
                            registre = RegistreEntree.objects.all().filter(username=user1.userdata).last()
                            registre.fin = now
                            registre.save()
                            context ={
                                'user1': user1,
                                'duree_timer': 5,
                                'redirection_timer': ''
                            }
                            return render(request, 'clientapp/goodbye.html', context)

            else:
                if (user1.userdata.type == 'Fabmanager' or user1.userdata.type == 'Concierge'):

                    return redirect('adminpanel:log', badgetag=badgetag)
                else:
                    if user1.userdata.online == 0:
                        if user1.userdata.active == 0:
                            context = {
                                'msg': "Votre compte n'est pas activé.",
                                'duree_timer': 5,
                                'redirection_timer': ''
                            }
                            template = loader.get_template('clientapp/error.html')
                            return HttpResponse(template.render(context, request))

                        else:
                            user1.userdata.online = 1
                            user1.userdata.save()

                            now = make_aware(datetime.datetime.now())
                            registre = RegistreEntree(username=user1.userdata, debut=now)
                            registre.save()
                            msg = user1.userdata.message
                            user1.userdata.message = None
                            user1.save()

                            machines=Machine.objects.all()
                            listemachines=[]
                            for machine in machines:
                                droit= Droit.objects.filter(username=user1.userdata).filter(machineId=machine).get()
                                machine.droit=droit.typeAccess
                                if machine.libre == 0:
                                    registre = RegistreMachine.objects.all().filter(machineId=machine).last()
                                    machine.user = registre.username
                                listemachines.append(machine)
                            context = {
                                "user1":user1,
                                'msg':msg,
                                'duree_timer': 30,
                                'listemachines':listemachines,
                                'redirection_timer': ''
                            }
                            template = loader.get_template('clientapp/welcome.html')
                            return HttpResponse(template.render(context, request))


                    else:
                        user1.userdata.online = 0
                        user1.userdata.save()

                        now = make_aware(datetime.datetime.now())
                        registre = RegistreEntree.objects.all().filter(username=user1.userdata).last()
                        registre.fin = now
                        registre.save()
                        # return HttpResponse(user.userdata.online)
                        # return render(request, 'clientapp/goodbye.html', {'user1': "1"})
                        context = {
                            "user1": user1,
                            'duree_timer': 5,
                            'redirection_timer': ''
                        }
                        template = loader.get_template('clientapp/goodbye.html')
                        return HttpResponse(template.render(context, request))

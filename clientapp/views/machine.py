
from django.http import HttpResponse
from django.template import loader
from django.views import View
import datetime
from fabaccess.models import *
from django.utils.decorators import method_decorator
from adminpanel.MesDecorateurs import *
from django.utils.timezone import make_aware

#affichage des machines
class ListeMachines(View):


    def get(self,request):
            machines=Machine.objects.all()
            tabmachines=[]
            for machine in machines:
                if machine.libre==0:
                    registre = RegistreMachine.objects.all().filter(machineId=machine).last()
                    machine.user=registre.username.user
                panne = Panne.objects.all().filter(machineId=machine).last()
                if panne:
                    if not panne.fin:
                        machine.panne = 1
                        machine.pannetype = panne.type
                tabmachines.append(machine)
            context = {
                'machines': tabmachines,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('clientapp/machines.html')

            return HttpResponse(template.render(context, request))



#Déclaration d'une panne
class DeclarationPanne(View):
    @method_decorator(estunUser())
    def dispatch(self, *args, **kwargs):
        return super(DeclarationPanne, self).dispatch(*args, **kwargs)


    def get(self,request,pk):
            machine=Machine.objects.get(pk=pk)
            context = {
                'machine':machine,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('clientapp/panne.html')
            return HttpResponse(template.render(context, request))

    def post(self,request,pk):
            machine=Machine.objects.get(pk=pk)
            detail = request.POST['cause']
            now = make_aware(datetime.datetime.now())
            type= request.POST['type']
            panne = Panne(machineId=machine, type=type, status='declare', debut=now, cause=detail)
            panne.save()
            machine.operationel = 0
            machine.libre = 1
            machine.save()
            return redirect('clientapp:machineuser')

class PanneView(View):
    @method_decorator(estunUser())
    def dispatch(self, *args, **kwargs):
        return super(PanneView, self).dispatch(*args, **kwargs)

    def get(self, request, pk):
        machine = Machine.objects.get(pk=pk)
        panne = Panne.objects.filter(machineId=machine).last()
        context = {
            'panne': panne,
            'machine': machine,
            'duree_timer': 60,
            'redirection_timer': 'panel/logout'
        }
        template = loader.get_template('clientapp/panneview.html')
        return HttpResponse(template.render(context, request))

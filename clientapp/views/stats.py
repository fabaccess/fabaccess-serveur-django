
from django.views import View
from django.http import HttpResponse
from django.template import loader
from fabaccess.models import *
import time
from django.utils.decorators import method_decorator
from adminpanel.MesDecorateurs import *
# Create your views here.



#vue pour afficher les statistiques d'utilisation

class Stats(View):
    @method_decorator(estunUser())
    def dispatch(self, *args, **kwargs):
        return super(Stats, self).dispatch(*args, **kwargs)

    def get(self,request):
        user=request.user
        tabmachines=[]
        machines=Machine.objects.all()
        for machine in machines:
            registres = RegistreMachine.objects.filter(username=user.userdata).filter(machineId=machine).all()
            total=0
            for registre in registres:
                if registre.fin:
                    total = total + (int(time.mktime(registre.fin.timetuple()))) - (
                        int(time.mktime(registre.debut.timetuple())))
            total=total/60/60
            total = round(total, 0)
            machine.total=total
            tabmachines.append(machine)
        regentres= RegistreEntree.objects.filter(username=user.userdata).all()
        total=0
        for regentre in regentres:
            if regentre.fin:
                total = total + (int(time.mktime(regentre.fin.timetuple()))) - (int(time.mktime(regentre.debut.timetuple())))

        presence=round((total/60/60),0)
        context={
            'machines':tabmachines,
            'presence':presence,
            'duree_timer': 60,
            'redirection_timer': 'panel/logout'
        }
        template= loader.get_template('clientapp/stats.html')
        return HttpResponse(template.render(context, request))


from django.http import HttpResponse
from django.template import loader
from django.views import View
from django.utils.decorators import method_decorator
from adminpanel.MesDecorateurs import *


#affiche le profil de l'utilisateur connecté et permet de le modifié
class Profil(View):
    @method_decorator(estunUser())
    def dispatch(self, *args, **kwargs):
        return super(Profil, self).dispatch(*args, **kwargs)

    def get(self,request):
            context = {
                'user': request.user,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('clientapp/profile.html')

            return HttpResponse(template.render(context, request))

    def post(self,request):
            user=request.user
            user.last_name=request.POST['Nom']
            user.first_name=request.POST['Prenom']
            user.email=request.POST['Email']
            user.save()
            # return HttpResponse(user.first_name)
            return redirect('clientapp:user')

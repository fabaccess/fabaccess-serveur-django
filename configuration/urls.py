"""configuration URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from rest_framework import routers
from fabaccess import views
from django.views.decorators.csrf import csrf_exempt
from rest_framework_swagger.views import get_swagger_view
from django.conf import settings
from django.conf.urls.static import static

router = routers.DefaultRouter()
router.register(r'user', views.UserViewSet)
router.register(r'userdata', views.UserdataViewSet)
router.register(r'machine', views.MachineViewSet)
router.register(r'registremanchine', views.RegistreMachineViewSet)
router.register(r'registreentree', views.RegistreEntreeViewSet)
router.register(r'panne', views.PanneViewSet)
router.register(r'droit', views.DroitViewSet)

schema_view = get_swagger_view(title='Fabacces API')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('clientapp.urls')),
    path('api/',include(router.urls)),
    path('api/', include('rest_framework.urls', namespace='rest_framework')),
    path(r'machine/init', csrf_exempt(views.InitialisationMachine.as_view()), name='init'),# mac ->nom type
    path(r'machine/debut', csrf_exempt(views.DebutUtilisation.as_view()), name='deb'),# bage mac- autorisation message
    path(r'machine/aide', csrf_exempt(views.Passagedumanager.as_view()), name='aide'),#badge idadmin, max -> autorisation message
    path(r'machine/fin', csrf_exempt(views.FinUtilisation.as_view()), name='fin'),#bage mac- autorisation message
    path(r'machine/veriffin', csrf_exempt(views.VerifFinUtilisation.as_view()), name='verifin'),#bage mac- autorisation message pour verifier les droit de fin
    path('panel/', include('adminpanel.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    url(r'docapi/', schema_view),
]

if settings.DEBUG: # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



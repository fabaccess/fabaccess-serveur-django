"""
WSGI config for configuration project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
import yaml

from django.core.wsgi import get_wsgi_application

server_settings={}
try:
    with open('/etc/django/settings-fabaccess.yaml', 'r') as f:
        server_settings = yaml.load(f)
except FileNotFoundError:
    print('No local settings.')
    pass

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'configuration.settings')

application =  get_wsgi_application()

# **Application FabAccess**

#### Requirements :

##### PYTHON 3.X:

Python 2 n'est pas compatible avec cette application


###### Django 2.2

Framework Python,
Utilisé pour l'api et le server web.


###### Django-rest-swagger 2.2.0
###### Djangorestframework 3.9.3

Plugin pour la création de l'api REST Django et sa documentation associée.
Consultez /docapi pour plus d'information

###### psycopg2-binary 2.8.2

Pour se connecter à la base de donnée postgres

###### Pillow 6.0.0

Gestion des images

Toutes les dépendances associées peuvent être installées avec pip. Un virtualenv est fortement conseillé.

Commande pour installer : **pip install -r requirements.txt**

###### Des nodesMCU
Mis sur chacunes des machines, ils appeleront l'application jdango. Dès qu'ils seront connectés sur internet, la machine associée sera rajoutée dans le panel admin.

###### Un ordinateur avec un lecteur RFID
Mis à disposition du public. Les utilisateurs pourront se connecter avec pour prouver leur présence dans les locaux et leur adhésion à jour.

Les administrateurs eux pourront acceder au panneau d'administration et gerer le parc et les utilisateurs.

#### Installation rapide

Se connecter à Postgres et créer une base de donnée 'fabaccess'. Vous pouvez créer un utilisateur unique pour cette base de donnée.

`sudo -u postgres -i`

`psql`

`create user fabaccess with password 'motdepasse';`

`CREATE DATABASE fabaccess;`

`alter role postgres SET client_encoding TO 'utf8';`

`alter role postgres  SET default_transaction_isolation TO 'read committed';`

`alter role postgres SET timezone TO 'UTC';`

`grant all privileges on database fabaccess to fabaccess;
`

`\q
`

`exit`

Vous devez configurer apache pour qu'il utilise l'application Django. Pour plus d'information:

https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/modwsgi/


Ensuite configurez django dans le fichier settings.py pour y mettre vos mots de passe. Puis créer votre compte administrateur.

`python3 manage.py createsuperuser `

Votre server est donc configuré et prêt à recevoir les requetes des nodesmcu et du rasberry.

Pour pouvoir utiliser l'importation dolibarr vous devez aller dans les options du panel admin.

Renseignez l'url de l'api dolibarr _http://.../api/index.php/members_ et la clef api que vous pouvez generer dans votre panel admin de dolibarr.

Attention : les utilisateurs Dolibarr créés sont mis _inactifs_ pour eviter de surcharger votre panel. Vous devez les activer dans le panel admin pour pouvoir leur assigner un badge.

#### Carte du site :

/ --> application utilisateur accessible sur le client RFID

/docapi --> Documentation de l'api

/api --> Api

/panel -->Panneau d'administration accessible sur le client RFID et sur un ordinateur classique. La fonction d'ajout de badge est accessible uniquement avec le RFID.




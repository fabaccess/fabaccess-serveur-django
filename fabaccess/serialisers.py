from rest_framework import serializers
from .models import Userdata, Machine, RegistreMachine, RegistreEntree, Droit, Panne
from django.contrib.auth.models import User



class UserSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model= User
        fields = ('url', 'username', 'email', 'groups', 'is_superuser')

class UserdataSerialiser(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Userdata
        fields  = ('user', "doliid", 'tagbadge', 'type', "message", "online", "active", 'subscription')

class MachineSerialiser(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Machine
        fields = ('nom', 'macboite', 'description', 'type', 'img','descriptionLongue','cout')

class RegistreMachineSerialiser(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = RegistreMachine
        fields= ('machineId', 'username', 'debut', 'fin')

class RegistreEntreeSerialiser(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = RegistreEntree
        fields= ('username', 'debut','fin')

class DroitSerialiser(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Droit
        fields= ('machineId', 'username', 'typeAccess')

class PanneSerialiser(serializers.HyperlinkedModelSerializer):

    class Meta:
        model= Panne
        fields=('machineId','debut', 'fin', 'cause')

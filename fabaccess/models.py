from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.




class Userdata(models.Model):
    """
    table pour ajouter librement des donnés au users
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    doliid=models.CharField(max_length=50, default='none')
    tagbadge=models.CharField(max_length=50)
    type=models.CharField(max_length=50, default='user')
    message=models.CharField(max_length=250, default='none',null=True,blank=True)
    online=models.IntegerField( default=0)
    active=models.IntegerField( default=1)
    subscription=models.IntegerField( default=0)
    random=models.CharField(max_length=10,null=True,blank=True)

    def __str__(self):
        return self.user.username

@receiver(post_save, sender=User)
def update_user_userdata(sender, instance, created, **kwargs):
    if created:
        Userdata.objects.create(user=instance)
    instance.userdata.save()

class Machine(models.Model):
    """
    table des machines
    """
    nom=models.CharField(max_length=100)
    macboite=models.CharField(max_length=250)
    description=models.CharField(max_length=250)
    operationel=models.IntegerField( default=1)
    libre=models.IntegerField(default=1)
    type=models.CharField(max_length=250)
    img=models.ImageField(upload_to='images/',null=True,blank=True)
    descriptionLongue=models.TextField(max_length=500,null=True,blank=True)
    cout=models.CharField(max_length=1000,null=True,blank=True)

    def __str__(self):
        return self.nom

class IMG(models.Model):
    """
    table de test
    """
    titre=models.CharField(max_length=250)
    image=models.FileField(upload_to='images/')

    def __str__(self):
        return self.titre

class RegistreEntree(models.Model):
    """
    registre des entrées
    time correspond au temps unix
    """
    username=models.ForeignKey(Userdata, on_delete=models.CASCADE)
    debut=models.DateTimeField(null=True)
    fin=models.DateTimeField(null=True)

class RegistreMachine(models.Model):
    """
    registre utilisation machine
    """
    machineId=models.ForeignKey(Machine, on_delete=models.CASCADE)
    username=models.ForeignKey(Userdata, on_delete=models.CASCADE)
    debut=models.DateTimeField(null=True)
    fin=models.DateTimeField(null=True)


    def __str__(self):
        return self.machineId.nom


typePanne=[
    (0,'Non gênant'),
    (1,'Gênant'),
    (2,'Hors service'),
]
statusPanne=[
    ('declare','Déclarée'),
    ('confirme','Confirmée'),
    ('attente','En Attente'),
    ('regle','Reglée'),
]

class Panne(models.Model):
    """
    registre des pannes
    """
    machineId=models.ForeignKey(Machine, on_delete=models.CASCADE)
    debut=models.DateTimeField(null=True)
    fin=models.DateTimeField(null=True)
    type=models.IntegerField(choices=typePanne,default=0)
    status=models.CharField(max_length=20,choices=statusPanne, default='regle')

    cause=models.CharField(max_length=250)
    def __str__(self):
        return self.cause

class Droit(models.Model):
    """
    table de jointure entre machine et user pour leur assigner les droits
    """
    machineId=models.ForeignKey(Machine, on_delete=models.CASCADE)
    username=models.ForeignKey(Userdata, on_delete=models.CASCADE)
    typeAccess=models.IntegerField()

    def __str__(self):
        return self.machineId.nom


class Script(models.Model):
    """
    table pour des variables utilisé dans le code
    """
    type=models.CharField(max_length=100)
    variable=models.CharField(max_length=100)

    def __str__(self):
        return self.type

class SiteOption(models.Model):
    """
    table pour les options du site, une seule entrée
    """
    nomDuFatlab=models.CharField(max_length=200,null=True,blank=True)
    logo=models.ImageField(upload_to='images/',null=True,blank=True)

class ClefAPI(models.Model):
    """
    table des variable pour l'api
    """
    nom=models.CharField(max_length=100)
    clef=models.CharField(max_length=250)

    def __str__(self):
        return self.nom
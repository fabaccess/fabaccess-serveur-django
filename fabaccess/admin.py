from django.contrib import admin

# Register your models here.
from .models import Userdata, Machine , RegistreEntree, RegistreMachine, Panne, Droit, Script, IMG, ClefAPI



admin.site.register(RegistreEntree)
admin.site.register(RegistreMachine)


@admin.register(Userdata)
class UserdataAdmin(admin.ModelAdmin):
    search_fields = ['user__username']
    list_display = ['user','type']
    list_filter = ['type', 'active']


@admin.register(Machine)
class MachineAdmin(admin.ModelAdmin):
    search_fields = ['nom']
    list_display = ['nom','type']


@admin.register(Panne)
class PanneAdmin(admin.ModelAdmin):
    list_display = ['machineId', 'cause']
    list_filter = ['machineId']


@admin.register(Script)
class ScriptAdmin(admin.ModelAdmin):
    list_display = ['type','variable']


@admin.register(ClefAPI)
class ClefAPIAdmin(admin.ModelAdmin):
    list_display = ['nom', 'clef']

@admin.register(Droit)
class DroitAdmin(admin.ModelAdmin):
    list_display = ['machineId','username','typeAccess']
    list_filter = ['machineId']
# Generated by Django 2.2 on 2019-05-24 09:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fabaccess', '0014_auto_20190524_0945'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='panne',
            name='timedebut',
        ),
        migrations.RemoveField(
            model_name='panne',
            name='timefin',
        ),
        migrations.RemoveField(
            model_name='registremachine',
            name='timedebut',
        ),
        migrations.RemoveField(
            model_name='registremachine',
            name='timefin',
        ),
        migrations.AlterField(
            model_name='panne',
            name='debut',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='panne',
            name='fin',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='registremachine',
            name='debut',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='registremachine',
            name='fin',
            field=models.DateTimeField(null=True),
        ),
    ]

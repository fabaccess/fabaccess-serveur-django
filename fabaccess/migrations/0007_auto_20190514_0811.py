# Generated by Django 2.2 on 2019-05-14 08:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fabaccess', '0006_auto_20190510_0801'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='img',
            name='machine',
        ),
        migrations.AddField(
            model_name='img',
            name='titre',
            field=models.CharField(default='meh', max_length=250),
            preserve_default=False,
        ),
    ]

# Generated by Django 2.2 on 2019-05-10 08:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fabaccess', '0005_auto_20190509_1514'),
    ]

    operations = [
        migrations.AddField(
            model_name='panne',
            name='debut',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='panne',
            name='fin',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='registreentree',
            name='debut',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='registreentree',
            name='fin',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='registremachine',
            name='debut',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='registremachine',
            name='fin',
            field=models.DateField(null=True),
        ),
    ]

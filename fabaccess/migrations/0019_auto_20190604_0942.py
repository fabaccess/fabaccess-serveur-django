# Generated by Django 2.2 on 2019-06-04 07:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fabaccess', '0018_auto_20190529_1719'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userdata',
            name='message',
            field=models.CharField(blank=True, default='none', max_length=250, null=True),
        ),
    ]


from fabaccess.models import *
from django.contrib.auth.models import User
from django.views import View
import datetime
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt
from django.utils.timezone import make_aware

# route pour le node mcu


# Debut de l'utilisation de la machine
# Id machine, id badge reçu
# Verification des droits, si fabmanager droit infinis
# return autorisation, et message
class DebutUtilisation(View):
    @csrf_exempt
    def post(self, request):
        rep=json.load(request)
        now = make_aware(datetime.datetime.now())
        machine= Machine.objects.get(macboite=rep['idmachine'])
        if (machine.libre==0):
            return JsonResponse({'autorisation':0, 'message': 'Machine deja occupée'})
        elif (machine.operationel==0):
            util = Userdata.objects.filter(tagbadge=rep['idbadge'])
            if not util:
                return JsonResponse({'autorisation': 0, 'message': 'Badge inconnu, allez voir le Fabmanager'})
            else:
                util = Userdata.objects.filter(tagbadge=rep['idbadge']).get()
                if (util.type == 'Fabmanager'):
                    return JsonResponse({'autorisation': 4, 'message': 'Attention machine en panne'})
                else:
                    return JsonResponse({'autorisation': 8, 'message': 'Machine en panne'})

        else:
            util= Userdata.objects.filter(tagbadge=rep['idbadge'])
            if not util:
                return JsonResponse({'autorisation': 0, 'message': 'Badge inconnu, allez voir le Fabmanager'})
            else:
                util = Userdata.objects.filter(tagbadge=rep['idbadge']).get()
                droi = util.droit_set.get(machineId=machine.id)
                if (util.active == 0):
                    return JsonResponse({'autorisation': 0, 'message': 'Compte inactif, allez voir le Fabmanager'})
                elif (util.type == 'Fabmanager'):
                    registre = RegistreMachine(machineId=machine, username=util, debut=now)
                    registre.save()
                    machine.libre = 0
                    machine.save()
                    return JsonResponse({"autorisation": 4, 'message': 'Bienvenue manager'})
                # non actif hors prod
                elif (util.online==0):
                    return JsonResponse({'autorisation': 1, 'message': "Présence non signalée, badgez à l'acceuil"})
                elif (droi.typeAccess == 0):
                    return JsonResponse({'autorisation': 1, 'message': 'Utilisation non autorisée'})
                elif (droi.typeAccess == 1):
                    return JsonResponse({'autorisation': 2, 'message': "Appelez le Fabmanager"})
                else:
                    registre = RegistreMachine(machineId=machine, username=util, debut=now)
                    user = User.objects.filter(userdata__tagbadge__exact=rep['idbadge']).get()
                    registre.save()
                    machine.libre = 0
                    machine.save()
                    return JsonResponse({"autorisation": 4, 'message': 'Bienvenue '+user.first_name})

#seconde requete pour verifier si le fabmanager autorise l'acces à la machine
class Passagedumanager(View):
    @csrf_exempt
    def post(self, request):
        rep=json.load(request)
        now = make_aware(datetime.datetime.now())
        machine= Machine.objects.get(macboite=rep['idmachine'])
        if (machine.libre == 0):
            return JsonResponse({'autorisation': 0, 'message': 'machine deja occupée'})
        elif (machine.operationel == 0):
            return JsonResponse({'autorisation': 0, 'message': 'Machine en panne'})
        else:
            util= Userdata.objects.filter(tagbadge=rep['idbadge'])
            admin = Userdata.objects.filter(tagbadge=rep['idadmin'])
            if not util or not admin:
                return JsonResponse({'autorisation': 0, 'message': 'Badge inconnu, allez voir le Fabmanager'})
            else:
                util = Userdata.objects.filter(tagbadge=rep['idbadge']).get()
                admin = Userdata.objects.filter(tagbadge=rep['idadmin']).get()
                if (admin.type=='Fabmanager'):
                    registre = RegistreMachine(machineId=machine, username=util, debut=now)
                    registre.save()
                    user = User.objects.filter(userdata__tagbadge__exact=rep['idbadge']).get()
                    machine.libre = 0
                    machine.save()
                    return JsonResponse({"autorisation": 4, 'message': 'Bienvenue '+user.first_name})
                else:
                    return JsonResponse({'autorisation': 1, 'message': 'Appelez le Fabmanager'})



# Fin de l'utilisation de la machine
# Id machine, id badge reçu
# Verification des droits, si fabmanager droit infinis
# return autorisation, et message
class FinUtilisation(View):
    @csrf_exempt
    def post(self, request):
        rep=json.load(request)
        now = make_aware(datetime.datetime.now())
        machine= Machine.objects.get(macboite=rep['idmachine'])
        util = Userdata.objects.filter(tagbadge=rep['idbadge'])
        if not util:
            return JsonResponse({'autorisation': 0, 'message': 'Badge inconnu, allez voir le Fabmanager'})
        else:
            if (machine.libre == 1):
                return JsonResponse({'autorisation': 4, 'message': "Fin de l'utilisation"})
            else:
                registre = RegistreMachine.objects.all().filter(machineId=machine).last()
                util = Userdata.objects.get(tagbadge=rep['idbadge'])
                if (registre.username == util or util.type == 'Fabmanager' or util.type == 'Concierge'):
                    registre.fin=now
                    registre.save()
                    machine.libre = 1
                    machine.save()
                    return JsonResponse({'autorisation': 4, 'message': "Fin de l'utilisation"})
                else:
                    return JsonResponse({'autorisation': 0, 'message': "Machine dejà occupée"})

# Verifier si l'utilisateur à le droit d'eteindre la machine.
class VerifFinUtilisation(View):
    @csrf_exempt
    def post(self, request):
        rep=json.load(request)
        now = make_aware(datetime.datetime.now())
        machine= Machine.objects.get(macboite=rep['idmachine'])
        util = Userdata.objects.filter(tagbadge=rep['idbadge'])
        if not util:
            return JsonResponse({'autorisation': 0, 'message': 'Badge inconnu, allez voir le Fabmanager'})
        else:
            if (machine.libre == 1):
                return JsonResponse({'autorisation': 4, 'message': "Verification des droits"})
            else:
                registre = RegistreMachine.objects.all().filter(machineId=machine).last()
                util = Userdata.objects.get(tagbadge=rep['idbadge'])
                if (registre.username == util or util.type == 'Fabmanager' or util.type == 'Concierge'):
                    return JsonResponse({'autorisation': 4, 'message': "Verification des droits"})
                else:
                    return JsonResponse({'autorisation': 0, 'message': "Machine dejà occupée"})



# Activation de la machine
# Id machine reçu
# si la machine n'existe pas, création de cette dernière et attribution des droits par default pour tout les utilisateurs
# return type de machine
class InitialisationMachine(View):
    @csrf_exempt
    def post(self, request):
        rep=json.load(request)
        machine= Machine.objects.filter(macboite=rep['idmachine'])
        if not machine:
            machine = Machine(macboite=rep['idmachine'],type='fixe',description="à remplir",operationel=1, libre=1, nom="Nouvelle machine")
            machine.save()
            users = User.objects.all()
            for user in users:
                machine = Machine.objects.get(macboite=rep['idmachine'])
                userdata=Userdata.objects.get(user=user)
                droit=Droit(username=userdata, machineId=machine,typeAccess=0)
                droit.save()

        else:
            machine = Machine.objects.get(macboite=rep['idmachine'])
            if (machine.operationel==1):
                if machine.libre==0:
                    registre = RegistreMachine.objects.all().filter(machineId=machine).last()
                    if registre.fin==None:
                        now = make_aware(datetime.datetime.now())
                        registre.fin = now
                        registre.save()
                    machine.libre=1
                    machine.save()

        return JsonResponse({"nom": machine.nom, "type":machine.type})


from rest_framework import viewsets
from fabaccess.serialisers import *
from django.contrib.auth.models import User
from rest_framework import permissions


class UserViewSet(viewsets.ModelViewSet):
    """
        Prendre les utilisateurs
        """
    queryset = User.objects.all()
    serializer_class = UserSerialiser
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


class UserdataViewSet(viewsets.ModelViewSet):
    """
            Les données utilisateurs specifiques à l'application Fabmanager
            """
    queryset = Userdata.objects.all()
    serializer_class =  UserdataSerialiser
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


class MachineViewSet(viewsets.ModelViewSet):
    """
            Les machines installées du parc
            """
    queryset = Machine.objects.all()
    serializer_class = MachineSerialiser
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


class RegistreMachineViewSet(viewsets.ModelViewSet):
    """
     Le registre d'utilisation des machines
    """
    queryset = RegistreMachine.objects.all()
    serializer_class = RegistreMachineSerialiser
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


class RegistreEntreeViewSet(viewsets.ModelViewSet):
    """
    Le registre des entrées et sorties
    """
    queryset = RegistreEntree.objects.all()
    serializer_class = RegistreEntreeSerialiser
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


class DroitViewSet(viewsets.ModelViewSet):
    """
    Les droits utilisateurs par rapport au machine
    0=aucun droit
    1=besoin d'une assistance
    2=autonome
    """
    queryset = Droit.objects.all()
    serializer_class = DroitSerialiser
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


class PanneViewSet(viewsets.ModelViewSet):
    """
    Listes des pannes du parc
    """
    queryset = Panne.objects.all()
    serializer_class = PanneSerialiser
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)

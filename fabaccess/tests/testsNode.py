from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from fabaccess.models import *
from django.utils.encoding import force_text

import json


class NodeTests(TestCase):

    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('test', 'test@test.test', 'test')
        user.refresh_from_db()
        user.userdata.type = "Fabmanager"
        user.userdata.tagbadge= "TESTBADGE"
        user.userdata.online = 0
        user.save()

        machine = Machine(macboite='TEST', type='fixe', description='description', operationel=1, libre=1,
                          nom="Machine de test")
        machine.save()
        user = User.objects.get()
        machine = Machine.objects.get(macboite='TEST')
        userdata = Userdata.objects.get(user=user)
        droit = Droit(username=userdata, machineId=machine, typeAccess=0)
        droit.save()


    def tests_init_oldmachine(self):
        """
        test pour initialiser la machine
        :return:
        """
        response = self.client.post(reverse('init'), json.dumps({'idmachine':'TEST'}),content_type="application/json")
        self.assertEquals(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'nom': "Machine de test", "type":"fixe"}
        )

    def tests_deb_machine(self):
        """
        test pour demarrer la machine
        :return:
        """
        response=self.client.post(reverse('deb'), json.dumps({'idmachine':'TEST','idbadge':'TESTBADGE'}),content_type='application/json')
        self.assertEquals(response.status_code,200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"autorisation": 4, 'message': 'Bienvenue manager'}
        )

    def test_fin_machine(self):
        """
        test pour eteindre la machine
        :return:
        """
        response = self.client.post(reverse('fin'), json.dumps({'idmachine': 'TEST', 'idbadge': 'TESTBADGE'}), content_type='application/json')
        self.assertEquals(response.status_code,200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'autorisation': 4, 'message': "Fin de l'utilisation"}
        )
from django_cron import CronJobBase, Schedule
from .models import *
from django.utils.timezone import make_aware
import datetime

class MyCronJob(CronJobBase):
    RUN_AT_TIME = ['22:00']

    schedule = Schedule(run_at_times=RUN_AT_TIME)
    code = 'fabaccess.deco_utilisateur'


    def do(self):
        users_pas_deco=User.objects.filter(userdata__online__exact=1).all()
        tableau_mauvais_user=[]
        for user in users_pas_deco:
            user.userdata.online=0
            now = make_aware(datetime.datetime.now())
            registre = RegistreEntree.objects.all().filter(username=user.userdata).last()
            registre.fin = now
            registre.save()
            user.userdata.message="Vous n'avez pas signalé votre départ lors de votre dernière visite !"
            user.save()
            tableau_mauvais_user.append(user.first_name)


        machines_pas_eteintes=Machine.objects.filter(libre=0).all()
        for machine in machines_pas_eteintes:
            registre = RegistreMachine.objects.all().\
            filter(machineId=machine).last()
            now = make_aware(datetime.datetime.now())
            registre.fin = now
            registre.save()
            machine.libre = 1
            machine.save()

        return tableau_mauvais_user
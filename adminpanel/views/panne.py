
from django.views import View
import datetime
from django.utils.timezone import make_aware
from django.http import HttpResponse, JsonResponse
from django.template import loader
import calendar
from adminpanel.MesDecorateurs import *
from django.utils.decorators import method_decorator
from adminpanel.forms import *
import time

#vue pour ajouter une panne
class AjoutPanne(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(AjoutPanne, self).dispatch(*args, **kwargs)

    def get(self, request, pk):
            machine = Machine.objects.get(pk=pk)
            form=PanneForms()
            context = {
                'machine': machine,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/panne.html')
            return HttpResponse(template.render(context, request))


    def post(self, request, pk):
        detail=request.POST['cause']
        type=request.POST['type']
        machine=Machine.objects.get(pk=pk)
        now = make_aware(datetime.datetime.now())
        if detail=="":
            return redirect('adminpanel:panne', pk=pk)
        else:
            panne = Panne(machineId=machine,type=type, status='declare', debut=now, cause=detail)
            panne.save()
            if type==2:
                machine.operationel = 0
                machine.libre = 1
                machine.save()
            return redirect('adminpanel:index')


# vue pour visualiser, modifier une panne
class VoirPanne(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(VoirPanne, self).dispatch(*args, **kwargs)

    def get(self, request, pk):
            machine = Machine.objects.get(pk=pk)
            panne = Panne.objects.filter(machineId=machine).last()
            context = {
                'panne': panne,
                'machine': machine,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/panneview.html')
            return HttpResponse(template.render(context, request))


    def post(self, request, pk):
            machine = Machine.objects.get(pk=pk)
            panne = Panne.objects.filter(machineId=machine).last()
            cause = request.POST['cause']
            type = request.POST['type']
            status = request.POST['status']
            if type == '2':
                machine.operationel = 0
                machine.libre = 1
                machine.save()
            else:
                machine.operationel = 1
                machine.save()
            panne.cause = cause
            panne.type = type
            panne.status = status
            panne.save()
            return redirect('adminpanel:panneview', pk=pk)
#vue pour clore une panne

class ClorePanne(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(ClorePanne, self).dispatch(*args, **kwargs)

    def get(self, request, pk):
            now = make_aware(datetime.datetime.now())
            machine = Machine.objects.get(pk=pk)
            panne = Panne.objects.filter(machineId=machine).last()
            panne.status='regle'
            panne.fin=now
            panne.save()
            machine.operationel = 1
            machine.save()
            return redirect('adminpanel:index')


#fonction pour envoyé les stat d'utilisation
#get machine et type de stat voulue
class StatPanne(View):
    def get(self,request):
        para=request.GET['para']
        machine=request.GET['machine']
        today = datetime.date.today()
        seven= today - datetime.timedelta(days=6)
        if para=="week":
            i = 0
            stats = []
            label= []
            labelc=" "
            retenu=0
            while i <7:
                label.append(labelc)

                req = Panne.objects.filter(machineId=machine).filter(debut__day=seven.day)
                seven = seven + datetime.timedelta(days=1)
                if not req:
                    total=0
                else:
                    req.all()
                    total=0
                    for reqs in req:
                        if reqs.fin:
                            total=total+(int(time.mktime(reqs.fin.timetuple()))) - (int(time.mktime(reqs.debut.timetuple())))

                total = total + retenu
                if total >= 24 * 60 * 60:
                    retenu = total - (24 * 60 * 60)
                    total = 24 * 60 * 60
                else:
                    retenu = 0

                total=total/60
                total=round(total,0)
                stats.append(total)
                i += 1
            leg = "Minutes en panne"
        if para=='month':
            i=0
            stats=[]
            label = []
            labelc = " "
            retenu = 0
            month= today -datetime.timedelta(days=29)
            while i <30:
                label.append(labelc)

                req= Panne.objects.filter(machineId=machine).filter(debut__day=month.day)
                month = month+datetime.timedelta(days=1)
                if not req:
                    total=0
                else:
                    req.all()
                    total=0
                    for reqs in req:
                        if reqs.fin:
                            total=total+(int(time.mktime(reqs.fin.timetuple()))) - (int(time.mktime(reqs.debut.timetuple())))

                total = total + retenu
                if total >= 24 * 60 * 60:
                    retenu = total - (24 * 60 * 60)
                    total = 24 * 60 * 60
                else:
                    retenu = 0

                total=total/60
                total = round(total, 0)
                stats.append(total)
                i +=1
            leg="Minutes en panne"

        if para=='year':
            i=0
            stats=[]
            label=[]
            labelc=12
            year=1
            retenu = 0
            while i<12:
                label.append(labelc)
                labelc+=1
                req= Panne.objects.filter(fin__month=year).filter(machineId=machine)
                year=year+1
                if not req:
                    total=0
                else:
                    req.all()
                    total=0
                    for reqs in req:
                        if reqs.fin != 0:
                            total=total+(int(time.mktime(reqs.fin.timetuple()))) - (int(time.mktime(reqs.debut.timetuple())))
                total = total + retenu
                if total >= 24 * 60 * 60 * 30:
                    retenu = total - (24 * 60 * 60 * 30)
                    total = 24 * 60 * 60 * 30
                else:
                    retenu = 0

                total=total/60/60
                total=round(total, 2)
                stats.append(total)
                i+=1
            label=['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai','Juin', 'Juillet','Aout','Septembre','Octobre','Novembre','Decembre']
            leg="Heures en panne"
        return JsonResponse({'stats':stats, 'label':label, 'leg':leg}, safe=False)


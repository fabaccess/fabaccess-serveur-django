from django.views import View
from django.http import HttpResponse, HttpResponseForbidden
import json
import random
import string
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout, login
from fabaccess.models import *
from django.utils.decorators import method_decorator
from adminpanel.MesDecorateurs import *

# fonction de connection avec le badge admin
class Login(View):
    @csrf_exempt
    def get(self, request, badgetag):
        idbadge = badgetag
        user=User.objects.filter(userdata__tagbadge__startswith=idbadge).get()
        login(request, user)
        context = {
            'user1': user,
            'duree_timer': 5,
            'redirection_timer': 'panel/'
        }
        template = loader.get_template('adminpanel/bonjour.html')
        return HttpResponse(template.render(context, request))


# fonction de deconnection du panel admin
class Logout(View):

    def get(self, request):
        user=request.user
        logout(request)
        context = {
            'user1': user,
            'duree_timer': 5,
            'redirection_timer': ''
        }
        template = loader.get_template('adminpanel/aurevoir.html')
        return HttpResponse(template.render(context, request))


class VerifyOnline(View):
    def get(self,request):
        badge=request.GET['badgetag']
        user = User.objects.filter(userdata__tagbadge__startswith=badge)
        if user:
            user=user.get()
            if user.userdata.type=='User':
                if user.userdata.online==1:
                    return HttpResponse(1)
                else:
                    return HttpResponse(0)
            else:
                return HttpResponse(2)
        else:
            return HttpResponse(0)



# passage en attente de reception
class Attente(View):
    def get(self, request, pk):

        user=User.objects.get(pk=pk)
        letter = string.ascii_lowercase
        rand= ''.join(random.choice(letter)for i in range(10))
        user.userdata.random=rand
        user.save()
        context = {
            'user1': user,
            'duree_timer': 60,
            'redirection_timer': 'panel/logout'
        }
        template = loader.get_template('adminpanel/attente.html')
        return HttpResponse(template.render(context, request))


# classe pour traitement du badge
class EnregistrementBadge(View):
    def post(self, request):
        rep = json.load(request)
        pk=rep['id']
        user= User.objects.get(pk=pk)
        if user.userdata.random==rep['code']:
            verify=Userdata.objects.filter(tagbadge=rep['badge'])
            if not verify:
                user.userdata.tagbadge = rep['badge']
                user.save()
                return HttpResponse(200)
            else:
                user = User.objects.filter(userdata__tagbadge__startswith=rep['badge']).get()
                return HttpResponseForbidden('Badge déjà attribué à '+str(user))

        else:
            return HttpResponseForbidden('Problème de communication')




# option active ou non (uniquement dolibarr atm)
class Option(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(Option, self).dispatch(*args, **kwargs)

    def get(self,request):
            doliurl=ClefAPI.objects.get(nom='doliurl')
            doliurl=doliurl.clef
            dolibarr=ClefAPI.objects.get(nom='dolibarr')
            doliclef=dolibarr.clef
            doliactive=ClefAPI.objects.get(nom='doliactive')
            doliactive=doliactive.clef
            context = {
                'doliurl':doliurl,
                'doliclef':doliclef,
                'doliactive':doliactive,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/option.html')
            return HttpResponse(template.render(context, request))

    def post(self,request):
            doliurl = ClefAPI.objects.get(nom='doliurl')
            doliurl.clef=request.POST['doliurl']
            doliurl.save()
            dolibarr = ClefAPI.objects.get(nom='dolibarr')
            dolibarr.clef=request.POST['doliclef']
            dolibarr.save()
            doliactive = ClefAPI.objects.get(nom='doliactive')
            if "active" in request.POST:
                doliactive.clef=1
            else:
                doliactive.clef=0
            doliactive.save()
            return redirect('adminpanel:option')


from django.views import View
import datetime
from django.http import HttpResponse, JsonResponse
from django.template import loader
from fabaccess.models import RegistreMachine, Machine
from django.utils.decorators import method_decorator
from adminpanel.MesDecorateurs import *
import time







#fonction pour envoyé les stat d'utilisation
#get machine et type de stat voulue
class StatsUtilisationMachine(View):
    def get(self,request):
        para=request.GET['para']
        machine=request.GET['machine']
        today = datetime.date.today()
        seven= today - datetime.timedelta(days=6)
        if para=="week":
            i = 0
            stats = []
            label= []
            labelc=" "
            retenu=0
            while i <7:
                label.append(labelc)

                req = RegistreMachine.objects.filter(debut__day=seven.day).filter(machineId=machine)
                seven = seven + datetime.timedelta(days=1)
                if not req:
                    total=0
                else:
                    req.all()
                    total=0
                    for reqs in req:
                        if reqs.fin:
                            total=total+(int(time.mktime(reqs.fin.timetuple()))) - (int(time.mktime(reqs.debut.timetuple())))

                total=total+retenu
                if total>=24 * 60 * 60:
                    retenu=total-(24*60*60)
                    total=24*60*60
                else:
                    retenu=0
                total=total/60
                total=round(total,0)
                stats.append(total)
                i += 1
            leg = "Minutes d'utilisation"
        if para=='month':
            i=0
            stats=[]
            label = []
            labelc = " "
            retenu=0
            month= today -datetime.timedelta(days=29)
            while i <30:
                label.append(labelc)

                req= RegistreMachine.objects.filter(machineId=machine).filter(debut__day=month.day)
                month = month+datetime.timedelta(days=1)
                if not req:
                    total=0
                else:
                    req.all()
                    total=0
                    for reqs in req:
                        if reqs.fin:
                            total=total+(int(time.mktime(reqs.fin.timetuple()))) - (int(time.mktime(reqs.debut.timetuple())))

                total = total + retenu
                if total >= 24 * 60 * 60:
                    retenu = total - (24 * 60 * 60)
                    total = 24 * 60 * 60
                else:
                    retenu = 0

                total=total/60
                total = round(total, 0)
                stats.append(total)
                i +=1
            leg="Minutes d'utilisation"

        if para=='year':
            i=0
            stats=[]
            label=[]
            labelc=12
            retenu = 0
            year=1
            while i<12:
                label.append(labelc)
                labelc+=1
                req= RegistreMachine.objects.filter(fin__month=year).filter(machineId=machine)
                year=year+1
                if not req:
                    total=0
                else:
                    req.all()
                    total=0
                    for reqs in req:
                        if reqs.fin != 0:
                            total=total+(int(time.mktime(reqs.fin.timetuple()))) - (int(time.mktime(reqs.debut.timetuple())))

                total = total + retenu
                if total >= 24 * 60 * 60*30:
                    retenu = total - (24 * 60 * 60*30)
                    total = 24 * 60 * 60*30
                else:
                    retenu = 0

                total=total/60/60
                total=round(total, 2)
                stats.append(total)
                i+=1
            label=['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai','Juin', 'Juillet','Aout','Septembre','Octobre','Novembre','Decembre']
            leg="Heures d'utilisation"
        return JsonResponse({'stats':stats, 'label':label, 'leg':leg}, safe=False)



#affichage des graphiques
class VueStatistiques(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(VueStatistiques, self).dispatch(*args, **kwargs)

    def get(self,request, pk):
            machine=Machine.objects.get(pk=pk)
            context= {
                "pk":pk,
                'machine':machine,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/statsmachine.html')
            return HttpResponse(template.render(context, request))

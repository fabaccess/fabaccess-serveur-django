from django.shortcuts import render
from django.core.mail import send_mail
import requests
from django.views import View
from django.http import HttpResponse
from django.template import loader
from fabaccess.models import *
from adminpanel.forms import SignUpForm
import time
import calendar
from django.utils.decorators import method_decorator
from adminpanel.MesDecorateurs import *

# Create your views here.



#vous pour afficher l'ensemble des utilisateurs
class ListeUtilisateur(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(ListeUtilisateur, self).dispatch(*args, **kwargs)

    def get(self,request):
            us = request.user
            now = calendar.timegm(time.gmtime())
            utilisateurActif = User.objects.filter(userdata__active__exact=1).order_by(('-userdata__online')).all()


            doli = ClefAPI.objects.get(nom='doliactive')
            context = {
                'users': utilisateurActif,
                'now':now,
                'doliactive': doli.clef,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/userindex.html')
            return HttpResponse(template.render(context, request))


#vue pour rechercher un utilisateur
class RechercheUtilisateur(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(RechercheUtilisateur, self).dispatch(*args, **kwargs)

    def get(self, request):
            recherche=request.GET['q']
            users=User.objects.filter(first_name__istartswith=recherche).all()
            doli=ClefAPI.objects.get(nom='doliactive')
            now = calendar.timegm(time.gmtime())
            context= {
                'users':users,
                'doli':doli.clef,
                'now':now,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/userindex.html')
            return HttpResponse(template.render(context, request))

#vue pour voir un seul utilisateur et le modifier
class VueUtilisateur(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(VueUtilisateur, self).dispatch(*args, **kwargs)

    def get(self, request, pk):
            user = User.objects.get(pk=pk)
            userdata = Userdata.objects.get(user=user)
            doli=ClefAPI.objects.get(nom='doliactive')
            context = {
                'user1': user,
                'userdata1': userdata,
                'doliactive':doli,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/userview.html')
            return HttpResponse(template.render(context, request))

    def post(self, request, pk):
            us = request.user
            userdata = Userdata.objects.get(user=us)

            user=User.objects.get(pk=pk)
            userdata=Userdata.objects.get(user=user)
            nom=request.POST['Nom']
            prenom=request.POST['Prenom']
            email=request.POST['Email']
            doliid=request.POST['Doliid']
            message=request.POST['Message']
            type=request.POST['Type']
            if "active" in request.POST:
                if userdata.active==0:
                    password = User.objects.make_random_password(length=14)
                    user.set_password(password)
                    #mail pour nouveau password
                    html_message = loader.render_to_string('adminpanel/mail_nouveau_mdp.html',{
                        'user':user,
                        'password':password
                    })
                    send_mail(
                        'Nouveau mot de passe',
                        ('Votre mot de passe FabAccess a été changé. \n'+
                        'Voici vos nouveaux accès \n'+
                        "Nom d'utilisateur : "+user.username+"\n"+
                        "Mot de passe : "+password+"\n"+
                        "Merci d'utiliser nos services"),
                        'fabaccess@openscop.tech',
                        [user.email],
                        fail_silently=False,
                        html_message=html_message
                    )
                userdata.active = 1
            else:
                userdata.active=0
                userdata.tagbadge = ''
            user.first_name=prenom
            user.last_name=nom
            user.email=email
            user.save()

            userdata.doliid=doliid
            userdata.message=message
            userdata.type=type
            userdata.save()
            #return HttpResponse(request.POST['active'])
            return redirect('adminpanel:user', pk=user.pk)



#vue pour voir les droits d'un utilisateur et les modifier
class DroitUtilisateur(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(DroitUtilisateur, self).dispatch(*args, **kwargs)

    def get(self, request, pk):
            user=User.objects.get(pk=pk)
            userdata=Userdata.objects.get(user=user)
            droits=Droit.objects.all().filter(username=userdata)
            context={
                'droits':droits,
                'pk':pk,
                'user1':user,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template=loader.get_template('adminpanel/droit.html')

            return HttpResponse(template.render(context, request))

    def post(self, request, pk):
        user=User.objects.get(pk=pk)
        userdata = Userdata.objects.get(user=user)
        ndroits=request.POST
        for key, value in ndroits.items():
            if (key!='next'):
                droit = Droit.objects.get(username=userdata, machineId=key)

                droit.typeAccess = int(value)
                droit.save()

        #return HttpResponse(droit.typeAccess)
        return redirect('adminpanel:userdroit', pk=user.pk)



#vue pour afficher les statistiques d'utilisation

class StatUtilisateur(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(StatUtilisateur, self).dispatch(*args, **kwargs)

    def get(self,request,pk):
        user=User.objects.get(pk=pk)
        tabmachines=[]
        machines=Machine.objects.all()
        for machine in machines:
            registres = RegistreMachine.objects.filter(username=user.userdata).filter(machineId=machine).all()
            total=0
            for registre in registres:
                if registre.fin:
                    total += (int(time.mktime(registre.fin.timetuple()))) - (
                        int(time.mktime(registre.debut.timetuple())))

            total=total/60/60
            total = round(total, 0)
            machine.total=total
            tabmachines.append(machine)
        regentres= RegistreEntree.objects.filter(username=user.userdata).all()
        total=0
        for regentre in regentres:
            if regentre.fin:
                total += (int(time.mktime(regentre.fin.timetuple()))) - (int(time.mktime(regentre.debut.timetuple())))

        presence=round((total/60/60),0)
        context={
            'machines':tabmachines,
            'presence':presence,
            'user1':user,
            'duree_timer': 60,
            'redirection_timer': 'panel/logout'
        }
        template= loader.get_template('adminpanel/userstat.html')
        return HttpResponse(template.render(context, request))

# vue pour ajouter un utilisateur
class Ajoututilisateur(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(Ajoututilisateur, self).dispatch(*args, **kwargs)

    def get(self, request):
            form = SignUpForm()
            context = {
                'form': form,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/useradd.html')
            return HttpResponse(template.render(context, request))

    def post(self, request):
            form = SignUpForm(request.POST)
            if form.is_valid():
                user = form.save()
                user.refresh_from_db()
                user.userdata.email=form.cleaned_data.get('email')
                user.last_name=form.cleaned_data.get('nom')
                user.first_name=form.cleaned_data.get('prenom')
                user.userdata.type="User"
                user.userdata.online=0
                user.save()

                #droit
                machines=Machine.objects.all()
                for machine in machines:
                    droit=Droit(machineId=machine, username=user.userdata, typeAccess=0)
                    droit.save()

                return redirect('adminpanel:user',pk=user.pk)
            else:

                return render(request, 'adminpanel/useradd.html', {'form':form})


#fonction d'importation et de maj dolibarr
class ImportationDolibarr(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(ImportationDolibarr, self).dispatch(*args, **kwargs)

    def get(self,request):

            doliurl=ClefAPI.objects.get(nom='doliurl')
            clefAPI = ClefAPI.objects.get(nom='dolibarr')
            url=doliurl.clef
            if (doliurl.clef=='0' or clefAPI.clef=='0'):
                msg="Dolibarr non configuré"
            else:

                params = {'DOLAPIKEY': clefAPI.clef,'limit':1000}
                r = requests.get(url, params=params)
                if r.status_code==200:
                    adherents = r.json()
                    msg = "importation, mise à jour reussies !"
                    tst=[]
                    for adherent in adherents:
                        doliunique= adherent['lastname'] + adherent['id']
                        doliunique= doliunique.lower()
                        if (doliunique)!=None:

                            tst.append(adherent['login'])
                            util = Userdata.objects.filter(doliid=doliunique)
                            if not util:



                                user = User.objects.create_user(username=doliunique,
                                                                    password=adherent['pass_indatabase_crypted'])

                                user.refresh_from_db()
                                user.userdata.doliid = doliunique
                                user.email = adherent['email']
                                user.last_name = adherent['lastname']
                                user.first_name = adherent['firstname']
                                user.userdata.active = 0
                                user.userdata.type = 'User'
                                user.userdata.online = 0

                                # droits
                                machines = Machine.objects.all()
                                for machine in machines:
                                    droit = Droit(machineId=machine, username=user.userdata, typeAccess=0)
                                    droit.save()
                                if (adherent['last_subscription_date_end']):
                                    user.userdata.subscription = adherent['last_subscription_date_end']
                                user.save()
                            else:
                                user = User.objects.filter(userdata__doliid__startswith=doliunique).get()
                                if (adherent['last_subscription_date_end']):
                                    user.userdata.subscription = adherent['last_subscription_date_end']
                                user.email = adherent['email']
                                user.save()


                else:
                    msg="Dolibarr mal configuré, veuillez verifier l'adresse et la clef api"


            us = request.user
            now = calendar.timegm(time.gmtime())
            userdata = Userdata.objects.get(user=us)
            users = User.objects.filter(userdata__active__exact=1).all()
            inactiveusers = User.objects.filter(userdata__active__exact=0).all()
            doli = ClefAPI.objects.get(nom='doliactive')
            context = {
                'userdata': userdata,
                'users': users,
                'inactiveusers': inactiveusers,
                'now': now,
                'doliactive': doli.clef,
                'msg':msg,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/userindex.html')
            return HttpResponse(template.render(context, request))

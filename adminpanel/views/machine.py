
from django.views import View
from django.http import HttpResponse
from django.template import loader
from adminpanel.forms import *
from django.views.generic import CreateView # new
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from adminpanel.MesDecorateurs import *



#ListMachines du panel qui montre les machines


class ListMachines(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(ListMachines, self).dispatch(*args,**kwargs)


    def get(self,request):
            machines = Machine.objects.all()
            tabmachine=[]
            for machine in machines:
                if machine.libre==0:
                    registre = RegistreMachine.objects.all().filter(machineId=machine).last()
                    machine.user = registre.username.user

                panne = Panne.objects.all().filter(machineId=machine).last()
                if panne:
                    if not panne.fin:
                        machine.panne = 1
                        machine.pannetype = panne.type

                tabmachine.append(machine)
            context = {

                'machines': tabmachine,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template = loader.get_template('adminpanel/index.html')
            return HttpResponse(template.render(context, request))

# visualisation d'une machine et modification de cette dernière
class UneMachine(View):
    @method_decorator(estAdmin())
    def dispatch(self, *args, **kwargs):
        return super(UneMachine, self).dispatch(*args, **kwargs)

    def get(self,request,pk):
            machine=Machine.objects.get(pk=pk)
            if machine.libre==0:
                registre = RegistreMachine.objects.all().filter(machineId=machine).last()
                machine.user = registre.username
            panne = Panne.objects.all().filter(machineId=machine).last()
            if panne:
                if not panne.fin:
                    machine.panne = 1
                    machine.pannetype = panne.type
            form=Machintf(instance=machine)
            context ={
                'form':form,
                'machine':machine,
                'duree_timer': 60,
                'redirection_timer': 'panel/logout'
            }
            template=loader.get_template('adminpanel/machine.html')
            return HttpResponse(template.render(context, request))

    def post(self,request, pk):
            machine=Machine.objects.get(pk=pk)

            if machine.libre==0:
                registre = RegistreMachine.objects.all().filter(machineId=machine).last()
                machine.user = registre.username
            panne = Panne.objects.all().filter(machineId=machine).last()
            if panne:
                if not panne.fin:
                    machine.panne = 1
                    machine.pannetype = panne.type
            form=Machintf(request.POST, request.FILES, instance=machine)
            if form.is_valid():

                form.save()
                machine.type=request.POST['type']
                machine.descriptionLongue=request.POST['descriptionLongue']
                machine.save()
                return redirect('adminpanel:machine',pk=pk)
            else:
                context =  {
                    'form':form,
                    'machine':machine,
                    'duree_timer': 60,
                    'redirection_timer': 'panel/logout'
                }
                template=loader.get_template('adminpanel/machine.html')
                return  HttpResponse(template.render(context, request))


#test pour mettre des images
class IMGMachine(CreateView):
    model = IMG
    form_class = SendIMG
    template_name = 'adminpanel/test.html'
    success_url = reverse_lazy('adminpanel:index')

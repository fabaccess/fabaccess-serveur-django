from django import forms
from django.contrib.auth.forms import UserCreationForm
from fabaccess.models import *

class SignUpForm(UserCreationForm):
    email= forms.CharField(max_length=200)
    nom= forms.CharField(max_length=200)
    prenom= forms.CharField(max_length=200)

    class Meta:
        model = User
        fields = ('username','nom','prenom','email','password1','password2')


class SendIMG(forms.ModelForm):

    class Meta:
        model = IMG
        fields = ['titre','image']

class Machintf(forms.ModelForm):

    class Meta:
        model=Machine
        fields=('nom','description','img','cout')


class PanneForms(forms.ModelForm):

    class Meta:
        model=Panne
        fields=('machineId','type','status','cause')


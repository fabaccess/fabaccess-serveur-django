from django.urls import path
from adminpanel import views
from django.views.decorators.csrf import csrf_exempt


app_name = 'adminpanel'
urlpatterns = [
    path(r'', views.ListMachines.as_view(), name='index'),
    path(r'logout', views.Logout.as_view(), name='logout'),
    path(r'machine/<int:pk>', views.machine.UneMachine.as_view(), name="machine"),
    path(r'panne/<int:pk>', views.AjoutPanne.as_view(), name='panne'),
    path(r'panneview/<int:pk>', views.VoirPanne.as_view(), name='panneview'),
    path(r'pannefin/<int:pk>', views.ClorePanne.as_view(), name='pannefin'),

    path(r'userindex', views.ListeUtilisateur.as_view(), name='userindex'),
    path(r'usersearch', views.RechercheUtilisateur.as_view(), name='search'),
    path(r'userview/<int:pk>', views.VueUtilisateur.as_view(), name="user"),
    path(r'userstat/<int:pk>', views.StatUtilisateur.as_view(), name="userstat"),
    path(r'useradd', views.Ajoututilisateur.as_view(), name="useradd"),
    path(r'droit/<int:pk>', csrf_exempt(views.DroitUtilisateur.as_view()), name='userdroit'),



    path(r'log/<str:badgetag>', csrf_exempt(views.Login.as_view()), name='log'),
    path(r'attentebadge/<int:pk>', csrf_exempt(views.Attente.as_view()), name='attentebadge'),
    path(r'envoiedubadge', csrf_exempt(views.EnregistrementBadge.as_view()), name='enregistrement'),
    path(r'verifyonline', views.VerifyOnline.as_view(), name='verifyonline'),


    path(r'imgadd', views.IMGMachine.as_view(), name="image"),

    path(r'doliimport', views.ImportationDolibarr.as_view(), name="import"),

    path(r'option', views.Option.as_view(), name='option'),

    #test
    path(r'stat/<int:pk>', views.VueStatistiques.as_view(), name='chart'),
    path(r'statp', views.StatPanne.as_view(), name='chatpanne'),
    path(r'teststat', views.StatsUtilisationMachine.as_view(), name="test"),

]

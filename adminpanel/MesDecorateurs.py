
from django.shortcuts import redirect
from functools import wraps


def estAdmin(test=None):
    def _estAutorise(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            user=request.user
            if not user.is_authenticated:
                return redirect('login')
            if request.user.userdata.type=='Fabmanager' or request.user.userdata.type=='Concierge':
                return function(request, *args, **kwargs)
            else:
                return  redirect('clientapp:index')
        return wrapper
    return _estAutorise


def estunUser(test=None):
    def _estunUser(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            user=request.user
            if not user.is_authenticated:
                return redirect('login')
            if request.user.userdata.type=='User':
                return function(request, *args, **kwargs)
            else:
                return redirect('clientapp:index')
        return wrapper
    return _estunUser

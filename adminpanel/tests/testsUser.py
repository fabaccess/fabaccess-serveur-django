from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from fabaccess.models import *
# Create your tests here.


class UsersViewTests(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('test', 'test@test.test', 'test')
        user.refresh_from_db()
        user.userdata.type = "Fabmanager"
        user.userdata.online = 0
        user.save()

        machine = Machine(macboite='TEST', type='fixe',description='description',operationel=1, libre=1, nom="Machine de test")
        machine.save()
        user=User.objects.get()
        machine = Machine.objects.get(macboite='TEST')
        userdata = Userdata.objects.get(user=user)



    def tests_index_isco(self):
        """
        si ok si co
        :return:
        """
        User = get_user_model()
        self.client.login(username='test', password='test')
        response = self.client.get(reverse('adminpanel:userindex'))
        self.assertEquals(response.status_code, 200)

    def tests_index_ispasco(self):
        """
        si nan si pas co
        :return:
        """
        response =self.client.get(reverse('adminpanel:userindex'))
        self.assertEquals(response.status_code,302)

    def test_user_view(self):
        """
        on vois l'utilisateur
        :return:
        """

        user=User.objects.get(username='test')
        self.client.login(username='test', password='test')
        response=self.client.get(reverse('adminpanel:user',args=[user.pk]))
        self.assertEquals(response.status_code,200)
        self.assertEquals(response.context['user1'].username,'test')




    def test_droit_view(self):
        """
        vue pour les doits
        :return:
        """
        user=User.objects.get(username='test')
        self.client.login(username='test',password='test')
        response=self.client.get(reverse('adminpanel:userdroit',args=[user.pk]))
        self.assertEquals(response.status_code,200)








from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from fabaccess.models import *
import json
# Create your tests here.


class PanneViewTests(TestCase):
    def setUp(self):
        User = get_user_model()
        user=User.objects.create_user('test','test@test.test','test')
        user.refresh_from_db()
        user.userdata.type = "Fabmanager"
        user.userdata.online = 0
        user.save()

        machine = Machine(macboite='TEST', type='fixe', description='description', operationel=1, libre=1,
                          nom="Machine de test")
        machine.save()
        user = User.objects.get()
        machine = Machine.objects.get(macboite='TEST')
        userdata = Userdata.objects.get(user=user)
        droit=Droit.objects.filter(username=userdata)
        if not droit:
            droit = Droit(username=userdata, machineId=machine, typeAccess=0)
            droit.save()

    def tests_pann(self):
        """
        affichage du formulaire d'une panne
        :return:
        """
        self.client.login(username='test', password='test')
        machine=Machine.objects.get(macboite='TEST')
        response=self.client.get(reverse('adminpanel:panne',args=[machine.pk]))
        self.assertEquals(response.status_code,200)

    def tests_pann_post(self):
        """
        envoie de la panne
        :return:
        """
        self.client.login(username='test', password='test')
        machine=Machine.objects.get(macboite='TEST')
        response=self.client.post(reverse('adminpanel:panne',args=[machine.pk]),{'cause':'nuh','type':0,'status':0})
        self.assertEquals(response.status_code,302)

    def tests_panneview_get(self):
        """
        visu de la panne
        :return:
        """
        self.client.login(username='test',password='test')
        machine=Machine.objects.get(macboite='TEST')
        response=self.client.get(reverse('adminpanel:panneview',args=[machine.pk]))
        self.assertEquals(response.status_code,200)



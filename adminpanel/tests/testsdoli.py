from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from fabaccess.models import *
# Create your tests here.


class DoliViewTests(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('test', 'test@test.test', 'test')
        user.refresh_from_db()
        user.userdata.type = "Fabmanager"
        user.userdata.online = 0
        user.save()

        machine = Machine(macboite='TEST', type='fixe',description='description',operationel=1, libre=1, nom="Machine de test")
        machine.save()
        user=User.objects.get()
        machine = Machine.objects.get(macboite='TEST')
        userdata = Userdata.objects.get(user=user)


    def test_doliimport_des(self):
        """
        test doli
        :return:
        """
        user = User.objects.get(username='test')
        self.client.login(username='test', password='test')
        response=self.client.get(reverse('adminpanel:import'))
        self.assertEquals(response.status_code,200)
        self.assertEquals(response.context['msg'],"Dolibarr non configuré")
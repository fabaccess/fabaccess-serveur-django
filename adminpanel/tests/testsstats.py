from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from fabaccess.models import *

import json


class StatsTests(TestCase):

    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('test', 'test@test.test', 'test')
        user.refresh_from_db()
        user.userdata.type = "Fabmanager"
        user.userdata.tagbadge= "TESTBADGE"
        user.userdata.online = 0
        user.save()

        machine = Machine(macboite='TEST', type='fixe', description='description', operationel=1, libre=1,
                          nom="Machine de test")
        machine.save()
        user = User.objects.get()
        machine = Machine.objects.get(macboite='TEST')
        userdata = Userdata.objects.get(user=user)
        droit = Droit(username=userdata, machineId=machine, typeAccess=0)
        droit.save()


    def test_utilmachine(self):
        response=self.client.get(reverse('adminpanel:test'),{'para':'week','machine':"1"})
        self.assertEquals(response.status_code,200)
        response=self.client.get(reverse('adminpanel:test'),{'para':'month','machine':'1'})
        self.assertEquals(response.status_code,200)
        response = self.client.get(reverse('adminpanel:test'), {'para': 'year', 'machine': '1'})
        self.assertEquals(response.status_code, 200)

    def UtilMachineView(self):
        self.client.login(username='test', password='test')
        response=self.client.get(reverse('adminpanel:chart'))
        self.assertEquals(response.status_code,200)

from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from fabaccess.models import *
# Create your tests here.


class IndexViewTests(TestCase):
    def setUp(self):
        User = get_user_model()
        user=User.objects.create_user('test','test@test.test','test')
        user.refresh_from_db()
        user.userdata.type = "Fabmanager"
        user.userdata.online = 0
        user.save()

        machine = Machine(macboite='TEST', type='fixe', description='description', operationel=1, libre=1,
                          nom="Machine de test")
        machine.save()
        user = User.objects.get()
        machine = Machine.objects.get(macboite='TEST')
        userdata = Userdata.objects.get(user=user)
        droit=Droit.objects.filter(username=userdata)
        if not droit:
            droit = Droit(username=userdata, machineId=machine, typeAccess=0)
            droit.save()



    def test_machine_avecco(self):
        """
        test si on arrive au panel admin
        :return:
        """
        User = get_user_model()
        self.client.login(username='test', password='test')
        response =self.client.get(reverse('adminpanel:index'))
        self.assertEquals(response.status_code,200)

    def test_machine_sansco(self):
        """
        test si on arrive pas au panel admin car on est pas co
        :return:
        """
        response=self.client.get(reverse('adminpanel:index'))
        self.assertEquals(response.status_code,302)

    def test_unemachine(self):
        """
        affichage d'une machine
        :return:
        """
        User = get_user_model()
        self.client.login(username='test', password='test')
        machine = Machine.objects.get(macboite='TEST')
        response=self.client.get(reverse('adminpanel:machine', args=[machine.pk]))
        self.assertEquals(response.status_code,200)
        self.assertEquals(response.context['machine'],machine)

    def test_unemachine_post(self):
        """
        modification d'une machine
        :return:
        """
        User = get_user_model()
        self.client.login(username='test', password='test')
        machine = Machine.objects.get(macboite='TEST')
        response=self.client.post(reverse('adminpanel:machine',args=[machine.pk]),{"nom":'Machine de test','description':'meh','descriptionLongue':"remeh",'type':'fixe'})
        self.assertEquals(response.status_code,302)

# Fabaccess

## Utilisation

### NodeMCU.

Les nodes MCU doivent être compilé avec le code présent dans le repo https://gitlab.com/fabaccess/fabaccess-client-rpi.
Vous devez modifier le fichier 'secret.h' comme suivant

`// connection au wifi`

`#define SSID "votressid"`

`#define PSSW "votremotdepassewifi"`

`// connection a l'api`

`#define API "http://.../machine/"<- mettre l'adresse de votre site`

Recompilez et envoyez ce code dans vos nodemcu. 

Vous devez ensuite brancher vos nodemcu sur votre machine. Dès l'initialisation de celui-ci, l'application django créera une _nouvelle machine_. A vous de changer le nom et les paramètres dans le panneau d'administration.


### L'application d'acceuil

L'application d'acceuil pour être lancer sur un ordinateur possédant un lecteur RFID. Idéalement vous devriez utisé un rabserry py connecté avec un écran tactile.

Cette application permettra à vos utilisateurs de se connecter à l'entrée du Fablab, mais aussi au administrateur d'assigner les badges.

Vous trouverez les sources de ce dernier ici : https://gitlab.com/fabaccess/fabaccess-client-rpi

Vous devez modifier le fichier config.py en y rentrant l'adresse du server django. Vous avez example.config.py pour vous aider.

Attention si les utilisateurs ne se sont pas signalés à la borne d'entrée ils ne pourront pas utiliser les machines.

### L'application Django.

L'application Django permet de gerer le parc des machines ainsi que les utilisateurs. L'installation est précisée dans le fichier readme.

Vous pouvez vous connecter au panel admin avec vos codes personnels. Selon le niveau de l'utilisateur vous aurez accès au panneau d'utilisateur ou au panneau d'administration.

 ##### Point Dolibarr

L'option Dolibarr permet d'activer l'intégration Dolibarr, Elle permet essentiellement deux choses.

- L'importation des utilisateurs via Dolibarr. Nous importons directement vos adhèrents dans l'application, leur nom d'utilisateur est créé avec leur nom+l'id de leur compte. Ils peuvent le récuperer ainsi que créer le mot de passe via la fonction mot de passe oublié.
- La vérification du paiement des membres. Seuls les membres à jour de cotisations pourront utiliser les machines et badger à l'entrée. A noter cependant que les fabmanager et concierges passent outre  ce prérequis.

Attention ! Une fois l'option active, les simples utilisateurs créés manuellement ne pourront plus utiliser l'application. Vous devrez les rentrer dans Dolibarr.

Par ailleurs les membres sont importés, mais il vous faudra les activer manuellement et leur assigner une carte.

Cette option peut être désactivée dans le panneau d'administration. Dans ce cas, les utilisateurs importés ne sont pas supprimés, mais la fonction de verification d'adhésion n'est plus active.